const APIKEY = 'AIzaSyB7EMbKRTbR2mHSmSMkIIMZNmErmUhpuKc';
const altermativeCoordsUrl = 'https://ipinfo.io/geo';

const mode = '%22WALKING%22'; // 'mode=%22DRIVING%22';
const BASE_URL    = 'https://www.nhtsa.gov/webapi/api/SafetyRatings/',
      BASE_PARAMS = 'format=json';

// Request example:
// http://maps.googleapis.com/maps/api/directions/json?origin=-20.291825,57.448668&destination=-20.179724,57.613463&sensor=false&mode=%22DRIVING%22

let directionsURL = `http://maps.googleapis.com/maps/api/directions/json`;

const data  = {
    getCoordinates (origin, destination) {
        debugger;
        console.log("Request:: ", `${directionsURL}?origin=${origin}&destination=${destination}&sensor=false&mode=${mode}`);
        return fetch(`${directionsURL}?origin=${origin}&destination=${destination}&sensor=false&mode=${mode}`);
    },
    getAlternativeCoordinates () {
        return fetch(`${altermativeCoordsUrl}`);
    }
}

export default data;