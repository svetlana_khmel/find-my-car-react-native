import React, { Component } from 'react';
import { View, Text, Navigator, TouchableHighlight, PixelRatio, StyleSheet } from 'react-native';

export default class Button extends Component {

    constructor () {
        super();

        this.navigate = this.navigate.bind(this);
   }

    navigate (name) {
        this.props.navigator.push({name});
    }

    render () {
        let setStyle;

        if (this.props.styleType === 1) {
            setStyle = styles.regularButtonOne;
        } else if (this.props.styleType === 2) {
            setStyle = styles.regularButtonTwo;
        } else if (this.props.styleType === 3) {
            setStyle = styles.regularButtonThree;
        } else {
            setStyle = styles.button
        }

        console.log('Buttons props....', this.props);

        return (
            <TouchableHighlight  onPress={this.props.onPress} style={setStyle}>
               <Text style={styles.buttonText}>{this.props.val}</Text>
            </TouchableHighlight>
        )
    }
}

const styles = StyleSheet.create({
    button: {
        borderWidth: 1/PixelRatio.get(),
        borderColor: "#995aba",
        borderRadius: 10,
        backgroundColor: '#995aba',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 10,
        height: 40
    },
    buttonText: {
        fontSize: 14,
        color: "#fff",
        margin: 10
    },
    regularButtonOne: {
        borderWidth: 1/PixelRatio.get(),
        borderColor: "#995aba",
        borderRadius: 10,
        backgroundColor: '#995aba',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        width: 200
    },
    regularButtonTwo: {
        borderWidth: 1/PixelRatio.get(),
        borderColor: "#b56bdb",
        borderRadius: 10,
        backgroundColor: '#b56bdb',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        width: 200
    },
    regularButtonThree: {
        borderWidth: 1/PixelRatio.get(),
        borderColor: "#cf7ef9",
        borderRadius: 10,
        backgroundColor: '#cf7ef9',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        width: 200
    }
});