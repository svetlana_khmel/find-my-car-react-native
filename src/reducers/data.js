const initialState = {};

export default function cars(state = initialState, action) {
    switch (action.type) {

        case 'SET_ORIGIN_DATA' :
            console.log('SET_ORIGIN_DATA   ', action.coordinates);
            return {
                ...state,
                originCoordinates : action.coordinates,
                makes     : action.makes
            };

         case 'SET_DESTINATION_DATA' :
            console.log('SET_DESTINATION_DATA   ', action.coordinates);
            return {
                ...state,
                coordinates : action.coordinates,
                makes     : action.makes
            };
         case 'SET_ALTERNATIVE_COORDS' :
            console.log('SET_ALTERNATIVE_COORDS   ', action.coordinates);
            return {
                ...state,
                coordinates : action.coordinates,
                makes     : action.makes
            };

         case 'SET_DIRECTIONS_COORDS' :
            console.log("SET_DIRECTIONS_COORDS  ", action.coordinates);
            return {
                ...state,
                directionCoordinates : action.coordinates,
                makes     : action.makes
            };

        default:
            return state;
    }
};