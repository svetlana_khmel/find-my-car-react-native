import data from '../data';
import { Reducer } from 'redux-testkit';

const initialState = {}

const SET_ORIGIN_DATA = {
                          "geocoded_waypoints": [
                            {
                              "geocoder_status": "OK",
                              "place_id": "ChIJMeqT1rXF1EARKuTqVp7NuzY",
                              "types": [
                                "street_address"
                              ]
                            },
                            {
                              "geocoder_status": "OK",
                              "place_id": "ChIJMeqT1rXF1EARKuTqVp7NuzY",
                              "types": [
                                "street_address"
                              ]
                            }
                          ],
                          "routes": [
                            {
                              "bounds": {
                                "northeast": {
                                  "lat": 50.3937316,
                                  "lng": 30.6185354
                                },
                                "southwest": {
                                  "lat": 50.3937316,
                                  "lng": 30.6185354
                                }
                              },
                              "copyrights": "Map data ©2017 Google",
                              "legs": [
                                {
                                  "distance": {
                                    "text": "1 m",
                                    "value": 0
                                  },
                                  "duration": {
                                    "text": "1 min",
                                    "value": 0
                                  },
                                  "end_address": "Yelyzavety Chavdar St, 1А, Kyiv, Ukraine, 02000",
                                  "end_location": {
                                    "lat": 50.3937316,
                                    "lng": 30.6185354
                                  },
                                  "start_address": "Yelyzavety Chavdar St, 1А, Kyiv, Ukraine, 02000",
                                  "start_location": {
                                    "lat": 50.3937316,
                                    "lng": 30.6185354
                                  },
                                  "steps": [
                                    {
                                      "distance": {
                                        "text": "1 m",
                                        "value": 0
                                      },
                                      "duration": {
                                        "text": "1 min",
                                        "value": 0
                                      },
                                      "end_location": {
                                        "lat": 50.3937316,
                                        "lng": 30.6185354
                                      },
                                      "html_instructions": "Head",
                                      "polyline": {
                                        "points": "yoqrH{d{yD"
                                      },
                                      "start_location": {
                                        "lat": 50.3937316,
                                        "lng": 30.6185354
                                      },
                                      "travel_mode": "DRIVING"
                                    }
                                  ],
                                  "traffic_speed_entry": [],
                                  "via_waypoint": []
                                }
                              ],
                              "overview_polyline": {
                                "points": "yoqrH{d{yD"
                              },
                              "summary": "",
                              "warnings": [],
                              "waypoint_order": []
                            }
                          ],
                          "status": "OK"
                        }

 const SET_ALTERNATIVE_COORDS = {
                                "ip": "46.219.223.169",
                                "city": "Kiev",
                                "region": "Kyiv City",
                                "country": "UA",
                                "loc": "50.4333,30.5167"
                              }

describe('Should return the initial state', () => {
    it('Shouls have initial state',  () => {
        expect(data().toEqual(initialState))
    });

    it('Should not affect state', () => {
        Reducer(data).expect({type: 'NOT_EXISTING'}).toReturnState(initialState);
    });

    it('should get SET_ORIGIN_DATA', () => {
        const action = {type: 'SET_ORIGIN_DATA', SET_ORIGIN_DATA};
        Reducer(data).expect(action).toReturnState({...initialState, SET_ORIGIN_DATA});
    });

    it('should get SET_ALTERNATIVE_COORDS', () => {
        const action = {type: 'SET_ALTERNATIVE_COORDS', SET_ALTERNATIVE_COORDS};
        Reducer(data).expect(action).toReturnState({...initialState, SET_ALTERNATIVE_COORDS});
    });
});