import { combineReducers } from "redux";
import dataReducer from "./data";

export default function getRootReducer(navReducer) {
    return combineReducers({
        nav: navReducer,
        dataReducer: dataReducer
    });
}