import { Thunk } from 'redux-testkit';
import data from '../../actions/data';
import getData from '../../data/data';
jest.mock('../../data/data');

 const SET_ALTERNATIVE_COORDS = {
                                "ip": "46.219.223.169",
                                "city": "Kiev",
                                "region": "Kyiv City",
                                "country": "UA",
                                "loc": "50.4333,30.5167"
                              }

describe('store/topics/actions', () => {

  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should fetch topics from server', async () => {
    getData.getAlternativeCoordinates.mockReturnValueOnce(SET_ALTERNATIVE_COORDS);
    const dispatches = await Thunk(data.fetchAlternativeCoords).execute();
    expect(dispatches.length).toBe(1);
    expect(dispatches[0].isPlainObject()).toBe(true);
    expect(dispatches[0].getAction()).toEqual({ type: SET_ALTERNATIVE_COORDS, coordinates: SET_ALTERNATIVE_COORDS});
  });
});