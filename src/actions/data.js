import data from '../data/data';

function setCoordinates(coordinates, makes) {
    return {
        type : 'SET_ORIGIN_DATA',
        coordinates,
        makes
    };
};

function setAlternativeCoordinates(coordinates, makes) {
    return {
        type : 'SET_ALTERNATIVE_COORDS',
        coordinates,
        makes
    };
};

function setDirectionCoords(coordinates, makes) {
    return {
        type : 'SET_DIRECTIONS_COORDS',
        coordinates,
        makes
    };
};

export function fetchCoordinates(origin, destination) {
    return function (dispatch, getState) {
        return data.getCoordinates(origin, destination)
            .then((response) => response.json())
            .then((data) => dispatch(setCoordinates(data)))
            .catch((err) => console.log(err));
    };
};

export function fetchAlternativeCoords() {
    return function (dispatch, getState) {
        return data.getAlternativeCoordinates()
            .then((response) => response.json())
            .then((data) => dispatch(setAlternativeCoordinates(data)))
            .catch((err) => console.log(err));
    };
};

export function fetchDirections() {
    return function () {
        return data.getDirections()
            .then((response) => response.json())
            .then(() => dispatch(setDirectionCoords(data)))
            .catch((err) => console.log(err));
    }
}

