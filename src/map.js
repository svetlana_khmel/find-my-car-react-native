import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  PixelRatio
} from 'react-native';

import MapView, { Marker, Circle } from 'react-native-maps';
import Polyline from '@mapbox/polyline';
import pick from 'lodash/pick';

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as dataActions from "./actions/data";

import Button from './components/button'

var { width, height } = Dimensions.get('window');

//To calculate latiudeDelta/longDelta
const earthRadiusInKM = 6371;
// you can customize these two values based on your needs
const radiusInKM = 0.1;
const aspectRatio = 0.1;

class Map extends Component {
    constructor () {
        super();

        this.state = {
            region: {
               latitude: 0,
               longitude: 0,
               latitudeDelta: 0,
               longitudeDelta: 0,
             },
             destination: {},
             markers: [],
             carMarketSet: false,
             routeCoordinates: [],
             distanceTravelled: 0,
             prevLatLng: {},
             circles: [{
                  center: {
                    latitude: 0,
                    longitude: 0
                  },
                  MapCircle: {
                      latitude: 0,
                      longitude: 0
                  },
                  radius: 50,
                  fillColor: '#2196f3',
                  strokeColor: '#f44336'
             }],
             polylines: [

             ],
             currentPosition: {
                latitude: 0,
                longitude: 0,
             },
             polylineCoordiantes: [],
             coords: [],
             distance: ''
        };

        this.centerMe = this.centerMe.bind(this);
        this.centerCar = this.centerCar.bind(this);
        this.showRoutes = this.showRoutes.bind(this);
        this.getCurrentPosition = this.getCurrentPosition.bind(this);
        this.getWatchPosition = this.getWatchPosition.bind(this);
        this.removeMarker = this.removeMarker.bind(this);
        //this.decode = this.decode.bind(this);
       // this.getCoordinates = this.getCoordinates.bind(this);

        this.addMarker = this.addMarker.bind(this);

        this.deg2rad = this.deg2rad.bind(this);
        this.rad2deg = this.rad2deg.bind(this);
        this.showRegion = this.showRegion.bind(this);

        //this.getDirections = this.getDirections.bind(this);
        //this.setDataToPoliline = this.setDataToPoliline.bind(this);
        this.getAlternativeCoords = this.getAlternativeCoords.bind(this);
    }

    static navigationOptions = {
          title: 'Map',
    };

    componentDidMount () {
        console.log("Component did mount....");
        this.getCurrentPosition();
    }

    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.watchID);
    }

    showRoutes (origin, destination) {
       this.props.fetchCoordinates(origin, destination).then(data => {

           console.log("....................getCoordinates  ", this.props.data.dataReducer.originCoordinates.routes[0]);

           if (this.props.data.dataReducer.originCoordinates.routes[0]) {

               debugger;

               let points = Polyline.decode(this.props.data.dataReducer.originCoordinates.routes[0].overview_polyline.points);

               let coords = points.map((point, index) => {
                   return  {
                       latitude : point[0],
                       longitude : point[1]
                   }
               })

               this.setState({
                 coords: coords,
                 distance: this.props.data.dataReducer.originCoordinates.routes[0].legs[0].distance.text
               });

               return coords;
           }
        });
    }

    getCurrentPosition (setMarker) {
        console.log('____ getCurrentPosition');

        navigator.geolocation.getCurrentPosition((position) => {

            console.log("Position... ", position);
            let coords = {
                longitude: position.coords.longitude,
                latitude: position.coords.latitude
            }

            this.setState({
               region: {
                    longitude: coords.longitude,
                    latitude: coords.latitude,
                    latitudeDelta: this.state.region.latitudeDelta,
                    longitudeDelta: this.state.region.longitudeDelta
                },
                destination: {
                    longitude: coords.longitude,
                    latitude: coords.latitude
                }
             });

             if (setMarker === true) {
                 this.addMarker({coords});
             }

             this.getWatchPosition();
        }, (error) => {
            console.log(JSON.stringify(error));

            this.getAlternativeCoords(setMarker);
        }, {
            maximumAge:60000,
            timeout:1000,
            enableHighAccuracy: true
        });
    }

    getAlternativeCoords (setMarker) {
        console.log("MAP PROPS", this.props);

        this.props.fetchAlternativeCoords().then(response => {
               let loc = response.loc ? response.loc.split(',') : response.coordinates.loc.split(',');
               let coords = {
                   latitude: parseFloat(loc[0]),
                   longitude: parseFloat(loc[1])
               };

               this.showRegion(coords);

               if (setMarker === true) {
                   this.addMarker(coords);
               }
            });
    }

    getWatchPosition () {

        console.log('___ getWatchPosition');

        navigator.geolocation.watchPosition((position) => {

            const { routeCoordinates } = this.state
            const positionLatLngs = pick(position.coords, ['latitude', 'longitude']);

            this.setState({
                routeCoordinates: routeCoordinates.concat(positionLatLngs),
            });

            this.showRegion({longitude: position.coords.longitude, latitude: position.coords.latitude});


            //this.setDataToPoliline() // Draw directions
        }, (error) => {
            this.getAlternativeCoords();
            console.log(JSON.stringify(error));
        }, {
            enableHighAccuracy: true
        });
    }

    showRegion(locationCoords) {
        var radiusInRad = radiusInKM / earthRadiusInKM;
        var longitudeDelta = this.rad2deg(radiusInRad / Math.cos(this.deg2rad(locationCoords.latitude)));
        var latitudeDelta = aspectRatio * this.rad2deg(radiusInRad);

        this.setState({
          region: {
                longitude: locationCoords.longitude,
                latitude: locationCoords.latitude,
                latitudeDelta: latitudeDelta,
                longitudeDelta: longitudeDelta
            },
            circles: [{
                latitude: locationCoords.longitude,
                longitude: locationCoords.latitude,
                radius: 50,
                fillColor: '#000',
                strokeColor: '#333',
                center: {
                    latitude: locationCoords.longitude,
                    longitude: locationCoords.latitude
                }
            }]
          });
    }

    deg2rad (angle) {
        return angle * 0.017453292519943295 // (angle / 180) * Math.PI;
    }

    rad2deg (angle) {
        return angle * 57.29577951308232 // angle / Math.PI * 180
    }

//    setDataToPoliline () {
//
//        this.props.fetchDirections().then(response => {
//                     let polylineCoordiantes = response.routes[0].geometry.coordinates.map(c => {
//                                      return {
//                                         latitude: c[1],
//                                         longitude: c[0],
//                                      }
//                                  });
//
//                                  this.setState({
//                                    polylineCoordiantes: polylineCoordiantes
//                                  })
//                      console.log('Get data for direction...', response);
//        });

    centerMe () {
        let map = [
            {
                latitude: this.state.region.latitude,
                longitude: this.state.region.longitude
            }
        ]

        this.map.fitToCoordinates(map, {
              animated: true,
        });
    }

    centerCar () {
        let map = [
            {
                latitude: this.state.region.latitude,
                longitude: this.state.region.longitude
            },
            {
                latitude: this.state.destination.coordinate.latitude,
                longitude: this.state.destination.coordinate.longitude
            }
        ]

        this.map.fitToCoordinates(map, {
              animated: true,
        });
    }

    addMarker (coords) {
        let origin = this.state.region.latitude +','+ this.state.region.longitude;
        //let destination = e.nativeEvent.coordinate.latitude +','+ e.nativeEvent.coordinate.longitude;
        let destination = this.state.destination.latitude +','+ this.state.destination.longitude;

        if (this.state.markers.length != 1) {
            this.setState({
                    markers: [
                       // ...this.state.markers, //Because we need only one marker
                        {
                           // coordinate: e.nativeEvent.coordinate,
                           coordinate: this.state.region
                           // cost: `$${getRandomInt(50,300)}`
                        }
                    ],
                    carMarketSet: true,
                    destination: {
                        coordinate: this.state.region
                    }
                });

                this.showRoutes(origin, destination);
        }
    }

    removeMarker () {
        //this.state.markers.length = 0;

        this.setState({
            markers: [],
            carMarketSet: false
        })
        console.log('....this.state.markers', );
    }

    render() {
        //const { region } = this.props;
        console.log(this.state.region);

        return (
          <View style={styles.container}>
            <View style={styles.buttonContainer}>
                  <Button val={'Where I am'} onPress={() => this.centerMe()} />

                  {this.state.carMarketSet === true &&
                    <Button val={'Where my car is?'} onPress={() => this.centerCar()} />
                  }

                  {this.state.carMarketSet === false &&
                    <Button val={'Add marker'} onPress={() => this.getCurrentPosition(true)} />
                  }

                  {this.state.carMarketSet === true &&
                      <Button val={'Remove marker'} onPress={() => this.removeMarker()} />
                  }
            </View>

            <MapView
              ref={ref => { this.map = ref; }}
              style={styles.map}
              region={this.state.region}
              showsUserLocation={true}
              followUserLocation={true} //map will focus on the user's location
              showsMyLocationButton={true}
              showsCompass={true}
              showsScale={true}
              showsTraffic={true}
              zoomEnabled={true}
              loadingEnabled={true}
              loadingIndicatorColor={'#606060'}
              overlays={[{
                  coordinates: this.state.routeCoordinates,
                  strokeColor: '#19B5FE',
                  lineWidth: 10,
                }]}
            >
                {this.state.markers.map((marker, index)=> {
                    return (
                        <Marker {...marker} style={styles.marker} key={index} draggable image={require('./images/Car.png')} onDragEnd={(e) => this.setState({ x: e.nativeEvent.coordinate })}>

                        </Marker>
                    )
                })}

                 <MapView.Polyline
                            coordinates={this.state.coords}
                            strokeWidth={5}
                            strokeColor="red"/>
            </MapView>

            <View style={styles.distanceView}>
                <Text style={styles.distance}>{this.state.distance}</Text>
            </View>
          </View>
        );
      }
}

export default connect(
    state => ({
        data: state
    }),
    dispatch => bindActionCreators(dataActions, dispatch)
)(Map);

//Map.navigationOptions = {
//  title: 'Add marker to mark your parking place',
//};

const styles = StyleSheet.create({
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },

  distanceView: {
    backgroundColor: 'red',
    position: 'absolute',
    left: 20,
    bottom: 40
  },

  distance: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: '#fff'
  },

  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  container: {
    ...StyleSheet.absoluteFillObject,
  },
  map: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 60,
    bottom: 0
  },
  button: {
    borderWidth: 1/PixelRatio.get(),
    borderColor: "#995aba",
    borderRadius: 10,
    backgroundColor: '#995aba',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
    height: 40
  },
  buttonText: {
    fontSize: 14,
    color: "#fff",
    margin: 10
  },
  marker: {
    padding: 5,
    borderRadius: 5
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 60
  }
});


