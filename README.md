# README #

*** The goal of the project:

** Learn how to works with maps in react native.

* Set marker

* Center on current position

* Center on route from current position to marker

* Set direction how to get from current point to marker

* Show distance

* Use redux as state manager

* Use thunk to fetch data

* Use DrawerNavigator, addNavigationHelpers from "react-navigation";


### Maps has been used: ###

https://github.com/airbnb/react-native-maps



### How to connect maps: ###

* 1) Install react-native-maps and create first component as on their example.

* 2) Then see "Troubleshoting" below!. (Clean cache, reinstall npm_modules, remove odd line from build.gradle thew was created by 'link', relink react-native-map )

*** Some troubleshoting commands: ***

* npm install --save react-native@latest

* android list sdk

* android list sdkmanager

* react-native upgrade

* watchman watch-del-all

* rm -rf node_modules && npm install

* rm -fr $TMPDIR/react-*` or `npm start -- --reset-cache

* rm -fr $TMPDIR/react-*

* npm start -- --reset-cache

* react-native start  in another tab


npm update -g npm

rm -rf node_modules

npm install




* 3) When project has been builded need to connect Google API key:

https://developers.google.com/maps/documentation/android-api/signup

Some steps than need to to to connect Google map API:

(How to generate keystore, generate SHA1 key see below;))


### For Android Studio: ###

Click on Build > Generate Signed APK.

You will get a message box, just click OK.

Now there will be another window just copy Key Store Path.

Now open a command prompt and go to C:\Program Files\Java\jdk1.6.0_39\bin> (or any installed jdk version).


Type keytool -list -v -keystore and then paste your Key Store Path (Eg. C:\Program Files\Java\jdk1.6.0_39\bin>keytool -list -v -keystore "E:\My Projects \Android\android studio\signed apks\Hello World\HelloWorld.jks").
Now it will Ask Key Store Password, provide yours and press Enter to get your SHA1 and MD5 Certificate keys.




**** If you are using Mac or even Linux, just copy and paste this onto the Terminal application and you will get the SHA1 key immediately. No need to change anything.

 keytool -list -v -keystore ~/.android/debug.keystore -alias androiddebugkey -storepass android -keypass android


### Troubleshoting ###

1) if Command `run-andriod` unrecognized::

npm install --save react-native@latest


The error is in the dependecies in build.gradle file try with this:

```
#!java


dependencies {
    compile fileTree(dir: "libs", include: ["*.jar"])
    compile "com.android.support:appcompat-v7:23.0.1"
    compile "com.facebook.react:react-native:+"  // From node_modules
    compile 'com.airbnb.android:react-native-maps:0.6.0'
}
```

Deleting the line compile project(':react-native-maps') the problem is resolved. This line is created by rnpm link but is a bug.

3) react-native upgrade


4) Google map API 

https://developers.google.com/maps/documentation/android-api/signup


5) if "Native component for "AIRMap" does not exist"


react-native unlink react-native-maps && react-native link react-native-maps


6) "This IP, site or mobile application is not authorized to use this API key"

On the url, it requires the server key in the end and not the api key for the app.

So Basically, you just add the server key in the end of the URL like this:

https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=yourlatitude,yourlongitude&radius=5000&sensor=true&key=SERVERKEY

Now, to obtain the server key, just follow these steps:

1) Go to Developer Console https://code.google.com/apis/console/

2) In the Credentials, under Public API Access , Create New key

3) Select the server key from the option.

4) Enter your IP Address on the field and if you have more ip addresses, you can just add on every single line.NOTE: Enter the IP Address only when you want to use it for your testing purpose. Else leave the IP Address section blank.

5) Once you are done, click create and your new Server Key will be generated and you can then add that server key to your URL.

Last thing is that, instead of putting the sensor=true in the middle of the URL, you can add it in the end like this:

https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=yourlatitude,yourlongitude&radius=5000&key=SERVERKEY&sensor=true

This will definitely solve the issue and just remember to use the server key for Places API.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact


###  Useful articles ###

https://medium.com/@lennyboyatzis/run-rabbit-run-path-tracking-with-react-native-and-the-geolocation-api-299227a9e241

https://developers.google.com/maps/documentation/directions/intro
https://github.com/airbnb/react-native-maps/blob/master/example/examples/FitToCoordinates.js

* ES6 console:

https://es6console.com/

*** Navigation with StackNavigator ***

https://reactnavigation.org/docs/intro/
https://www.dailydrip.com/topics/react-native/drips/getting-started-with-react-navigation

**** Maps: ***

https://developers.google.com/maps/

*** React Navigation and Redux in React Native Applications ***

http://moduscreate.com/react-navigation-redux-in-react-native-applications/


### Useful links: ###

* Geolocation:

https://facebook.github.io/react-native/docs/geolocation.html


*** To create direction ***

* You can use Google's or Mapbox's api to get an array of geo json coordinates that you can set on the coordinates prop for <Polyline />.

https://developers.google.com/maps/documentation/android-api/code-samples



Ok rm -rf .babelrc on the project root solved that problem for me. No idea why.


*** Unexpected Token @ when using babel decorator ***

You need to install babel-plugin-transform-decorators:

npm install babel-plugin-transform-decorators-legacy --save-dev

then add in .babelrc:

"plugins": ["transform-decorators-legacy"]


*** Direction with google: ***

https://medium.com/@ali_oguzhan/react-native-maps-with-google-directions-api-bc716ed7a366



**** Writing tests ****

https://github.com/reactjs/redux/blob/master/docs/recipes/WritingTests.md

https://facebook.github.io/jest/docs/tutorial-react-native.html

https://rajdee.gitbooks.io/redux-in-russian/content/docs/recipes/WritingTests.html

https://github.com/wix/react-dataflow-example/blob/e36e1f43ce797d47795eb15b44575f3e21912c7d/src/store/topics/__tests__/reducer.spec.js

https://github.com/wix/redux-testkit#installation


*** Methodology ***
https://hackernoon.com/redux-testing-step-by-step-a-simple-methodology-for-testing-business-logic-8901670756ce